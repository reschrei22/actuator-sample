package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActuatorSampleApplication {

	public static void main(String[] args) {
	    
	    String test = "demo";
	    
		SpringApplication.run(ActuatorSampleApplication.class, args);
	}
}
